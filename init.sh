#!/usr/bin/env bash

set -e
repo_base=/home/bga-repo

pretty_msg() {
  printf "\033[1;32m...oooOOO %s OOOooo...\033[0m\n" "$1"
}

# Minimum requis pour récupérer et construire les paquets sur une Arch brute
sudo pacman -Syu --needed --noconfirm base-devel git wget

if ! command -v yay > /dev/null; then
  pretty_msg "Installation de yay"
  [ -d /tmp/yay-bin ] || git -C /tmp clone --depth 1 https://aur.archlinux.org/yay-bin
  pushd /tmp/yay-bin
  git pull
  BUILDDIR=/tmp/makepkg makepkg -cfirs --needed --noconfirm
  popd
fi

pretty_msg "Création de la base locale des paquets et du paquet de base"
sudo install -d -o $USER -g alpm $repo_base
[ -d /tmp/arch-setup ] || git -C /tmp clone --depth 1 --single-branch https://gitlab.com/bergalath/arch-setup
pushd /tmp/arch-setup
git pull
BUILDDIR=/tmp/makepkg PKGDEST="$repo_base" makepkg -Ccfs --needed --noconfirm
popd
repo-add $repo_base/bergalath.db.tar.gz $repo_base/bergalath-*-any.pkg.tar.zst
yes | sudo pacman -U $repo_base/bergalath-desktop-*-any.pkg.tar.zst --needed
sudo pacman -Syu

pretty_msg "Installer d’autres paquets ?"
yay -S bergalath

pretty_msg "Pour l’instant, on est bon ! Prochaine étape, le bootstrap :"
echo
pretty_msg "  > bash -c 'source /usr/local/share/arch-setup/bootstrap.sh'"
echo
