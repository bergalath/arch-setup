# Maintainer: Bertrand Gauriat (bergalath) <bertrand@gauri.at>

pkgbase=bergalath
pkgname=(bergalath-desktop bergalath-laptop bergalath-work)
pkgdesc="Configurer mes systèmes basés sur ArchLinux"

pkgver=VERSION
pkgrel=1

packager=bergalath
arch=(any)
url="https://gitlab.com/bergalath/arch-setup"
license=(MIT)
groups=(bergalath)

makedepends=(base-devel git openssh wget xdg-user-dirs)

rootdir=$PWD
_dotbga="$(findmnt -n -o target LABEL="dotbga")"

check() {
  echo -e "\e[1;32m...oooOOO La clé USB est elle présente et montée ? OOOooo...\e[0m"
  [ -z "$_dotbga" ] && return 19
  echo -e "\e[1;32m...oooOOO Oui ! OOOooo...\e[0m"
}

pkgver() {
  cd "$rootdir"
  printf "r%s.%s" "$(git rev-list --count HEAD)" "$(date +%Y_%m_%d)"
}

package_bergalath-desktop() {
  provides=(nano vi vim)
  pkgdesc="Base commune minimale Bureau graphique (XFCE)"
  install=bergalath-desktop.install

  depends=(expac fish fisher neovim rsync starship vivid)

  # Ruby
  optdepends+=(ruby ruby-stdlib ruby-docs rust)

  # Docker
  optdepends+=(docker docker-buildx docker-compose dnsmasq pigz)

  # Glances
  # optdepends+=(hddtemp python-docker python-netifaces)

  # Les basiques
  optdepends+=(firefox-developer-edition viewnior)
  # openssl

  # Polices
  optdepends+=(noto-fonts noto-fonts-emoji noto-fonts-extra otf-firamono-nerd otf-monaspace-nerd
    ttf-dejavu-nerd ttf-firacode-nerd ttf-hack-nerd ttf-ibmplex-mono-nerd
    ttf-nerd-fonts-symbols-mono ttf-noto-nerd ttf-sourcecodepro-nerd)

  # Outils Sublime
  optdepends+=(sublime-text sublime-merge)

  # Pour XFCE/Thunar
  optdepends+=(gvfs-nfs)

  # Modern UNIX https://jvns.ca/blog/2022/04/12/a-list-of-new-ish--command-line-tools/
  optdepends+=(7zip bat dfc duf fd fzf fzy git-delta git-filter-repo lsd lshw lsof
    procs ripgrep sd tig tmux tree unzip wdiff zip)
  # broot curlie dog  dust glances glow httpie tldr unzip wdiff weechat zip

  # Autres
  optdepends+=(filezilla fwupd meld ghostty)
  # gnome-keyring transmission-remote-gtk)

  # Préparation des dossiers du package
  cp -r $rootdir/files-base/* $pkgdir

  # Réglages des permissions
  chmod 0750 $pkgdir/etc/sudoers.d

  install -D -t $pkgdir/usr/share/licenses/$pkgname/ $rootdir/LICENSE
}

package_bergalath-laptop() {
  pkgdesc="Base mobile"

  depends=(bergalath-desktop)

  optdepends+=(powertop)

  install -D -t $pkgdir/usr/share/licenses/$pkgname/ $rootdir/LICENSE
}

package_bergalath-work() {
  pkgdesc="Pour le travail"
  install=bergalath-work.install

  depends=(bergalath-laptop)

  # Ansible, plus besoin grâce à Docker
  # optdepends+=(ansible ansible-lint mitogen molecule molecule-plugins vagrant virtualbox virtualbox-guest-iso)

  optdepends+=(asciinema peek libgepub)

  # Préparation de WireGuard
  install -Dm0440 -t $pkgdir/etc/systemd/network/ $_dotbga/vpn/wg0.network
  install -Dm0440 -t $pkgdir/etc/systemd/network/ -o root -g systemd-network -m 640 $_dotbga/vpn/wg0.netdev

  install -D -t $pkgdir/usr/share/licenses/$pkgname/ $rootdir/LICENSE
}
