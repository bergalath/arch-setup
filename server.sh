#!/usr/bin/env bash

set -e

pretty_msg() {
  printf "\033[1;32m...oooOOO %s OOOooo...\033[0m\n" "$1"
}

sudo pacman -Syu --needed --noconfirm base-devel git

if ! command -v yay > /dev/null; then
  pretty_msg "Installation de yay"
  [ -d /tmp/yay-bin ] || git -C /tmp clone --depth 1 https://aur.archlinux.org/yay-bin
  pushd /tmp/yay-bin
  git pull
  BUILDDIR=/tmp/makepkg makepkg -cfirs --needed --noconfirm
  popd
fi

if [ -d ~/.local/dotfiles ]; then
  pretty_msg "Dotfiles déjà installés !"
else
  pretty_msg "Installation des dotfiles spéciale serveur"
  export GIT_SSH_COMMAND="ssh -l git -oHostname=gitlab.com"
  git clone --bare -b chii --single-branch gl:bergalath/dotfiles ~/.local/dotfiles && {
    pushd $_
    git --work-tree=$HOME checkout chii --force
    git config --local status.showUntrackedFiles no
    popd
  }
fi

pretty_msg "Changement de l’interpréteur de shell : fish"
command -v fish >/dev/null && chsh -s $(which fish)
