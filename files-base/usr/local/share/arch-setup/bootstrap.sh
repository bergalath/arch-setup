#
# Pour initialiser une machine fraîchement installée
#

pretty_msg() {
  printf "\033[1;32m...oooOOO %s OOOooo...\033[0m\n" "$1"
}

exist() {
  command -v "$1" > /dev/null
}

SETUP_DIR="$(dirname -- "$(readlink -f "${BASH_SOURCE}")")"

exist pacman-mirrors && {
  sudo pacman-mirrors --country Germany,France,Austria,Netherlands,Belgium --api --protocol https
  pretty_msg "Utiliser la branche instable de Manjaro ? [oy/N]"
  read ans && [[ ${ans:-N} =~ ^[yYoO]$ ]] && sudo pacman-mirrors --api -B unstable
  sudo pacman-mirrors --fasttrack 5
}
sudo pacman -Syu --needed --noconfirm

pretty_msg "Installation des paquets optionnels"
exist yay && for pkg in $(pacman -Qsq bergalath); do
  yay -S --needed --noconfirm $(expac -S "%o" ${pkg})
done

pretty_msg "Suppression des paquets inutiles"
for pkg in $(< ${SETUP_DIR}/rm_pkgs); do
  pacman -Qqe ${pkg} 2> /dev/null && sudo pacman -Rdcns ${pkg} --noconfirm
done

if [ -d ~/.local/dotfiles ]; then
  pretty_msg "Dotfiles déjà installés !"
else
  pretty_msg "Installation des dotfiles"
  # Branche en fonction de l’utilisation de la machine, sinon main par défaut
  branch_name="$(pacman -Qq bergalath-work > /dev/null && echo taf)"
  export GIT_SSH_COMMAND="ssh -l git -i $(findmnt -n -o target LABEL="dotbga")/ssh/id_bga -oHostname=gitlab.com"
  git clone --bare --branch ${branch_name:-main} --single-branch gl:bergalath/dotfiles ~/.local/dotfiles && {
    pushd $_
    git --work-tree=$HOME checkout ${branch_name:-main} --force
    git config --local status.showUntrackedFiles no
    popd
  }
fi

pretty_msg "Ajout des groupes à $USER"
sudo usermod -aG docker,games,http,users,wheel $USER

if exist VBoxManage ; then
  # VirtualBox est installé, machine hôte
  sudo usermod -aG vboxusers $USER
elif exist VBoxService ; then
  # VirtualBox n’est pas installé, mais les services sont disponibles, machine invitée
  sudo usermod -aG vboxsf $USER
fi

pretty_msg "Mise en place de Gnome Keyring via gcr-ssh-agent"
systemctl --user enable --now gcr-ssh-agent.service

pretty_msg "Installation des paquets AUR"
exist yay && for pkg in $(grep --invert-match '^#' ${SETUP_DIR}/aur_pkgs); do
  pretty_msg "Installer ${pkg} ? [oy/N]"
  read ans && [[ ${ans:-N} =~ ^[yYoO]$ ]] && yay -S --needed --noconfirm ${pkg}
done

pretty_msg "Nettoyage du cache des paquets"
exist yay && yay -S --clean --noconfirm

pretty_msg "Changement de l’interpréteur de shell : fish"
exist fish && chsh -s $(which fish)

pretty_msg "Tout est bon, se reconnecter pour tout activer !"
